$(function () {
  $('[data-toggle="tooltip"]').tooltip();
  $('[data-toggle="popover"]').popover();

  $("#modalContact").on("show.bs.modal", function (e) {
    console.log("modal ejecutandose, prueba");

    $("#contacto-btn").removeClass("btn-outline-success");
    $("#contacto-btn").addClass("btn-warning");
    //metodo para acceder a propiedades
    $("#contacto-btn").prop("disabled", true);
  });

  $("#modalContact").on("shown.bs.modal", function (e) {
    console.log("modal ejecutandose después, prueba");
  });

  $("#modalContact").on("hide.bs.modal", function (e) {
    console.log("modal se oculta, prueba");
  });

  $("#modalContact").on("hidden.bs.modal", function (e) {
    console.log("modal se ocultó, prueba");
    $("#contacto-btn").prop("disabled", false);
    $("#contacto-btn").removeClass("btn-warning");
    $("#contacto-btn").addClass("btn-outline-success");
  });
});

$(".carousel").carousel({
  interval: 4500,
});
