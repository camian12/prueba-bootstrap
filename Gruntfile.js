module.exports = function (grunt) {

  require('time-grunt')(grunt);
  require('jit-grunt')(grunt,{
      useminPrepare:'grunt-usemin'
  });



  grunt.initConfig({
    sass: {
      dist: {
        files: [{
            expand: true,
            cwd: 'css',
            src: ['*.scss'],
            dest: 'css',
            ext: '.css'
        }]
      }
    },

    watch:{
      files:['css/*.scss'],
      tasks:['css']
    },

    browserSync:{
      dev:{
        bsFiles:{ 
          src:[
            'css/*.css',
            '*.html',
            'js/*.js'
          ]
        },
        options:{
          watchTask:true,
          server: {
            baseDir:'./'
          }
        }
      }
    },

    imagemin:{
      dynamic:{
        files:[{
          expand:true,
          cwd:'./',
          src:'imgs/*.{png,gif,jpg,jpeg}',
          dest:'dist/'
        }]
      }
    },

    //copia archivos html a la carpeta dist
    copy:{
      html:{
        files:[{
          expand:true,
          dot:true,
          cwd: './',
          src:['*.html'],
          dest: 'dist'
        }]
      },
      
      html2:{
        files:[{
          expand:true,
          dot:true,
          cwd: 'views/',
          src:['*.html'],
          dest: 'dist/views'
        }]
      },

      fonts:{
        files :[{
          expand:true,
          dot:true,
          cwd:'node_modules/material-icons',
          src: ['iconfont/*.*'],
          dest: 'dist'
        }]
      }

    },    

    clean:{
      build:{
        src:['dist/']
      }
    },

    cssmin:{
      dist:{}
    },

    uglify:{
      dist:{}
    },

    //generar codigo para archivos no sean cacheables por el browser archivos nuevos
    filerev:{
      options:{
        encoding:'utf8',
        algorithm:'md5',
        lenght:20
      },

      release:{
        files:[{
          src:[
            'dist/js/*.js',
            'dist/css/*.css',
          ]
        }]
      }
    },

    concat:{
      options:{
        separator:';'
      },
      dist:{}
    },

    useminPrepare: {
      foo:{
        dest:'dist',
        src:['index.html','views/about.html','views/contactanos.html','views/galeria.html','views/precios.html']
      },
      options:{
        flow:{
          steps:{
            css:['cssmin'],
            js:['uglify']
          },
          post:{
            css:[{
              name:'cssmin',
              createConfig: function(context,block){
                var generated=context.options.generated;
                generated.options={
                  keepSpecialComments:0,
                  rebase:false
                }
              }
            }]
          }
        }
      }
    },

    usemin:{
      html:['dist/index.html','dist/views/about.html','dist/views/contactanos.html','dist/views/galeria.html','dist/views/precios.html'],
      options:{
        assetsDir:['dist', 'dist/css', 'dist/js']
      }
    }

  });


  grunt.registerTask('css', ['sass']);
  grunt.registerTask('default',['browserSync','watch']);
  grunt.registerTask('img:compress',['imagemin']);
  grunt.registerTask('build',[
    'clean',
    'copy',
    'imagemin',
    'useminPrepare',
    'concat',
    'cssmin',
    'uglify',
    'filerev',
    'usemin'
  ])
};


